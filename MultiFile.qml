import webprep2  1.0

WPFBase {

	//types: qml_write, wpfile, bin_copy, dir, multi_file
	type : "multi_file";
	property string source : "";
	property string postProcess: "";
	property string ppOut: "";
	
	//generate all pages by default
	property int first_page: 0;
	property int last_page: -1;
	property int getPage: -1;
	property string getMeta: "";
	property string target: composeName(getPage,getMeta);
	property string target_base: ""
	property string target_appendix: ""
	property string target_delimiter: "_"
						 
	function composeName(page, meta){
		return target_base + target_delimiter + meta + target_delimiter + page + target_appendix;
	}
	
}