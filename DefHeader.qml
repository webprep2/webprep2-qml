Element {
	property string pagetitle: "Title";
	property string pathPrefix : ""
	
	Element {
	c :  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\
     \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\
<html xmlns=\"http://www.w3.org/1999/xhtml\">\
\
<head>\
  <title>\
" + pagetitle + "\
  </title>\
\
  <meta http-equiv=\"Content-type\" content=\"text/xhtml; charset=UTF-8\" />\
\
  <link rel=\"stylesheet\" type=\"text/css\" href=\""+pathPrefix+"style.css\"/>\
  <style type=\"text/css\">\
    @import url("+pathPrefix+"stylemain.css);\
  </style>\
\
  <script type=\"text/javascript\" src=\""+pathPrefix+"script.js\">\
  </script>\
\
\
</head>\
\
\
<body>\
" }
}