
Element {
	id : self
	property alias pagetitle : head.pagetitle
	property string pathPrefix : ""
	
	property DefHeader head : DefHeader {
		id : head
		pathPrefix : self.pathPrefix
	}
	
	property DefFooter foot : DefFooter {
		pathPrefix : self.pathPrefix
	}
	
	property string result : head.result + foldedChildren + foot.result	
}