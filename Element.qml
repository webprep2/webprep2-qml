import Qt 4.7

QtObject {
	id : self
	//default gestattett (noch) kein list<>
	//property list<QtObject> children
	default property alias children : childrenHolder.children
	property QtObject childrenHolder : QtObject {
		property list<QtObject> children
		id : childrenHolder
	}

	property string c : ""
	property string foldedChildren 
	foldedChildren : {var res=""; for (var i=0;i<children.length;i++) res += children[i].result; res}
		
	property string result : foldedChildren + c
	
	function i(txt){
		var comp = Qt.createComponent("Emph.qml");
		while (!comp.status == Component.Ready);
		var emph1 = comp.createObject(self);
		emph1.c = txt;
		return emph1.result;
	}
	
	function b(txt){
		var comp = Qt.createComponent("Bold.qml");
		while (!comp.status == Component.Ready);
		var obj = comp.createObject(self);
		obj.c = txt;
		return obj.result;
	}
	
	function l(tgt,txt,tp){
		var comp = Qt.createComponent("Link.qml");
		while (!comp.status == Component.Ready);
		var obj = comp.createObject(self);
		obj.c = txt;
		obj.target = tgt; 
		obj.type = tp; 
		return obj.result;
	}
	
	

}