
Element {
	id : self
	property alias pagetitle : head.pagetitle
	property string pathPrefix : ""
	
	property Header head : Header {
		id : head
		pathPrefix : self.pathPrefix
	}
	
	property Footer foot : Footer {
		pathPrefix : self.pathPrefix
	}
	
	property string result : head.result + foldedChildren + foot.result	
}