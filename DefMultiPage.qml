
Element {
	id : self
	property alias pagetitle : head.pagetitle
	property string pathPrefix : ""
	
	property DefHeader head : DefHeader {
		id : head
		pathPrefix : self.pathPrefix
	}
	
	property DefFooter foot : DefFooter {
		pathPrefix : self.pathPrefix
	}
	
	property string result : head.result + deliverPage(getPage) + foot.result	
	
	property bool moreContent: true;
	property int getPage: -1;
	property string meta : deliverMeta(getPage);
	
	function deliverPage(num){
		return c;
	}
	
	function deliverMeta(num){
		return "p-" + num;
	}
}