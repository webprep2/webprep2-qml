Element {
	property string pagetitle: "Titel";
	property string pathPrefix : ""
	property string date : Qt.formatDateTime(new Date(), "dd. MMMM yyyy hh:mm:ss");
	
	Element {
	c :  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\
     \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\
<html xmlns=\"http://www.w3.org/1999/xhtml\">\
\
<head>\
  <title>\
  Frank Fuhlbrück .:. " + pagetitle + "\
  </title>\
\
  <meta http-equiv=\"Content-type\" content=\"text/xhtml; charset=UTF-8\" />\
\
  <link rel=\"stylesheet\" type=\"text/css\" href=\""+pathPrefix+"stylemain.css\"/>\
  <style type=\"text/css\">\
    @import url(stylemain.css);\
  </style>\
\
  <script type=\"text/javascript\" src=\""+pathPrefix+"script.js\">\
  </script>\
\
\
</head>\
\
\
<body>\
	<!--<object id=\"back\" data=\""+pathPrefix+"background.svg\" type=\"image/svg+xml\">\
	</object>-->\
	<div>\
\
\
\
	<div class=\"head\" id=\"hl\">\
		<a href=\"wasisneu.html\">letzte Änderung:</a> \
		" + 
		date
		+ " \
	</div>\
\
\
	<div class=\"head\" id=\"hr\">\
		<big>Frank&nbsp;Fuhlbrücks&nbsp;Seite</big> (Informatik-&nbsp;und&nbsp;Mathematikteil)\
	</div>\
\
<br/>\
\
	<div class='main' id='ml'>\n"
	}
	
	Link { target: pathPrefix + "index.html"; c:"Hauptseite"; newline:true }
	Link { target: pathPrefix + "ich.html"; c:"Kurzvorstellung"; newline:true }
	Link { target: pathPrefix + "kontakt.html"; c:"Kontakt"; newline:true }
	Link { target: pathPrefix + "proj.html"; c:"Projekte"; newline:true }
	Link { target: pathPrefix + "progs.html"; c:"Programme"; newline:true }
	Link { target: pathPrefix + "anl.html"; c:"Anleitungen"; newline:true }
	Link { target: pathPrefix + "maththi.html"; c:"Mathematisches"; newline:true }
	Link { target: pathPrefix + "kif.html"; c:"KIF"; newline:true }
	Link { target: pathPrefix + "links.html"; c:"Links"; newline:true }

	Element {c:"<br/>\n"}
	
	Link { target: "http://frafl.dyndns.org/frafl"; c:"private Interessen"; newline:true }
	
	Element {c:"<hr/>\n"}
	
	Bold {c:"Legende:"}
	Element {c:"\n<br/><br/>\n"}
	
	Link { target: pathPrefix + "links.html"; c:"interner Link"; newline:true }
	Link { target: pathPrefix + "links.html"; c:"externer Link"; newline:true; type: "extlink" }
	Link { target: pathPrefix + "links.html"; c:"Binärlink"; newline:true; type: "nonhtml" }
	

	Element {c:"<hr/>\n"}
	Bold {c:"Standards:"}
	Element {c:"\n<br/><br/>\n"}

	Link { target: "http://validator.w3.org/check?uri=referer"; c:"XHTML 1.0 Strict"; newline:true; type: "extlink" }
	
	Element {c:"
		Web &lt;= 1.96b
	</div>
	



	<div class=\"main\" id=\"mr\">
	" }
}